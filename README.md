## Only utils

It is a package with constants and utilities in python.

### Installation

```pip install only-utils```

[![pipeline status](https://gitlab.com/developerjoseph/only-utils/badges/master/pipeline.svg)](https://gitlab.com/developerjoseph/only-utils/-/commits/master) [![coverage report](https://gitlab.com/developerjoseph/only-utils/badges/master/coverage.svg)](https://gitlab.com/developerjoseph/only-utils/-/commits/master)