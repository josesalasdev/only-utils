from unittest import TestCase
from only_utils.utils.funtions import snake_case_to_camel


class TestCamelCaseToSnake(TestCase):

    def test_ok(self):
        result = snake_case_to_camel('hello_world')
        self.assertEqual(result, 'helloWorld')
